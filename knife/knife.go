package knife

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os/exec"
	"sort"
)

// Knife is an object that simplifies interacting with the knife command
type Knife struct {
	ChefDir string
	Cmd     []string
}

// New returns a new Knife object fill with default and environment values
func New(chefrepo string) Knife {
	return Knife{
		ChefDir: chefrepo,
		Cmd:     []string{"bundle", "exec", "knife"},
	}
}

// Run executes a knife command with the received arguments piping the output to the received writer
func (k Knife) Run(out io.Writer, command, query string, args ...string) error {
	knifeargs := append([]string{}, k.Cmd[1:]...)
	knifeargs = append(knifeargs, command, query)
	knifeargs = append(knifeargs, args...)

	cmd := exec.Command(k.Cmd[0], knifeargs...)
	cmd.Dir = k.ChefDir

	cmd.Stdout = out
	cmd.Stderr = out

	return cmd.Run()
}

// ResolveHostnames runs a knife search query to capture the host names and returns them in an array
func (k Knife) ResolveHostnames(query string) ([]string, error) {
	b := bytes.NewBuffer(nil)
	if err := k.Run(b, "search", query, "-F", "json"); err != nil {
		return nil, fmt.Errorf("could not resolve hostnames %s - %s", err, b.String())
	}
	hosts := chefHosts{}
	json.Unmarshal(b.Bytes(), &hosts)

	names := make([]string, len(hosts.Rows))
	for i, host := range hosts.Rows {
		names[i] = host.Name
	}
	sort.Strings(names)
	return names, nil
}

type chefHosts struct {
	Rows []struct {
		Name string `json:"name"`
	} `json:"rows"`
}
