package knife_test

import (
	"bytes"
	"fmt"
	"os"
	"strings"
	"testing"

	"gitlab.com/gl-infra/gitlab-patcher/knife"
)

func TestKnifeNew(t *testing.T) {
	wd, _ := os.Getwd()
	k := knife.New(wd)
	expectedCmd := []string{"bundle", "exec", "knife"}
	if strings.Join(k.Cmd, " ") != strings.Join(expectedCmd, " ") {
		t.Fatalf("Wrong command, expected %v, got %v", expectedCmd, k.Cmd)
	}
	if k.ChefDir != wd {
		t.Fatalf("Wrong working dir, expected %s, got %s", wd, k.ChefDir)
	}
}

func TestKnifeRun(t *testing.T) {
	wd, _ := os.Getwd()
	tt := []struct {
		name     string
		command  string
		args     []string
		expected string
		err      string
	}{
		{"simple command", "echo", []string{"hello", "hello", "hello"}, "hello hello hello\n", ""},
		{"false command", "bash", []string{"-c", "'bla'"}, "bash: bla: command not found\n", "exit status 127"},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			k := knife.Knife{
				Cmd:     []string{tc.command},
				ChefDir: wd,
			}

			out := bytes.NewBuffer(nil)
			err := k.Run(out, tc.args[0], tc.args[1], tc.args[2:]...)
			result := string(out.Bytes())
			if err != nil && fmt.Sprint(err) != tc.err {
				t.Fatalf("Failed to execute command, expected %s; got %s", tc.err, err)
			}
			if result != tc.expected {
				t.Fatalf("expected %s; got %s", tc.expected, result)
			}
		})
	}

}
