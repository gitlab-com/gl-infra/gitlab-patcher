package patcher

import (
	"io"

	"gitlab.com/gl-infra/gitlab-patcher/config"
	"gitlab.com/gl-infra/gitlab-patcher/knife"
)

// Patcher handles the whole patching process end to end
type Patcher struct {
	dry  bool
	cmds []func(io.Writer, knife.Knife) error
}

// Patch executes all the commands until one returns an error or they all
// complete
func (p Patcher) Patch(w io.Writer, k knife.Knife) error {
	for _, c := range p.cmds {
		if err := c(w, k); err != nil {
			return err
		}
	}
	return nil
}

// Command is a step in the patching process
type Command interface {
	Run(knife.Knife) error
	Desc() string
}

// New returns a new Patcher
func New(c config.Config) Patcher {
	p := Patcher{
		cmds: buildCommands(c),
	}
	return p
}
