package patcher

import (
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"path"
	"strings"

	"gitlab.com/gl-infra/gitlab-patcher/config"
	"gitlab.com/gl-infra/gitlab-patcher/knife"
)

func buildCommands(c config.Config) []func(io.Writer, knife.Knife) error {
	return []func(io.Writer, knife.Knife) error{
		header(c),
		resolve(c),
		scp(c),
		checksum(c),
		apply(c),
		hup(c),
		done(c),
	}
}

func header(c config.Config) func(io.Writer, knife.Knife) error {
	return func(w io.Writer, _ knife.Knife) error {
		fmt.Fprintf(w, "GitLab Patcher\n")
		fmt.Fprintf(w, "Mode: %s\n", c.Mode)
		fmt.Fprintf(w, "Patches: %s\n", strings.Join(c.GetPatchesToApply(), ", "))
		fmt.Fprintf(w, "Roles: %s\n", strings.Join(c.GetRolesToApply(), ", "))
		fmt.Fprintf(w, "------------------------------------\n")
		return nil
	}
}

func resolve(c config.Config) func(io.Writer, knife.Knife) error {
	return func(w io.Writer, k knife.Knife) error {
		fmt.Fprintf(w, "Resolving hostnames...\n")
		hostnames, err := k.ResolveHostnames(rolesQuery(c))
		if err != nil {
			return fmt.Errorf("could not resolve hostnames: %s", err)
		}
		fmt.Fprintf(w, "Resolved hosts: %s\n", strings.Join(hostnames, ", "))
		return nil
	}
}

func scp(c config.Config) func(io.Writer, knife.Knife) error {
	return func(w io.Writer, k knife.Knife) error {
		fmt.Fprintf(w, "Uploading patches...\n")
		checksums := []string{}

		for _, patchname := range c.GetPatchesToApply() {
			patch := c.Manifest.Patches[patchname]

			p := path.Join(c.WorkingDir, patchname)
			f, err := ioutil.ReadFile(p)
			if err != nil {
				return fmt.Errorf("could not read patch file %s: %s", p, err)
			}

			b64content := base64.StdEncoding.EncodeToString(f)

			checksums = append(checksums, fmt.Sprintf("%s  %s", patch.Checksum, patchname))
			cmd := fmt.Sprintf("echo '%s' | base64 -w 0 -di > %s", b64content, patchname)

			err = k.Run(w, "ssh", rolesQuery(c), cmd)
			if err != nil {
				return fmt.Errorf("failed to execute knife command %s", cmd)
			}
		}

		fmt.Fprintf(w, "Writing patches checksums file...\n")
		b64patches := base64.StdEncoding.EncodeToString([]byte(strings.Join(checksums, "\n")))

		cmd := fmt.Sprintf("echo '%s' | base64 -w 0 -di > patches.checksum", b64patches)

		err := k.Run(w, "ssh", rolesQuery(c), cmd)
		if err != nil {
			return fmt.Errorf("failed to execute knife command %s", cmd)
		}

		return nil
	}
}

func checksum(c config.Config) func(io.Writer, knife.Knife) error {
	return func(w io.Writer, k knife.Knife) error {
		fmt.Fprintf(w, "Verifying patches checksum...\n")
		err := k.Run(w, "ssh", rolesQuery(c), "md5sum -c patches.checksum")
		if err != nil {
			return fmt.Errorf("failed md5 checksum validation: %s", err)
		}
		return nil
	}
}

func apply(c config.Config) func(io.Writer, knife.Knife) error {
	return func(w io.Writer, k knife.Knife) error {
		fmt.Fprintf(w, "Applying patches in %s mode...\n", c.Mode)
		for _, patchname := range c.GetPatchesToApply() {
			p := c.Patches[patchname]

			var cmd string
			switch c.Mode {
			case config.ModeDryRun:
				cmd = fmt.Sprintf("(cd %s && sudo patch -f -p%d --dry-run) < %s", p.BaseDir, p.Level, patchname)
			case config.ModePatch:
				cmd = fmt.Sprintf("(cd %s && sudo patch -f -p%d) < %s", p.BaseDir, p.Level, patchname)
			case config.ModeRollback:
				cmd = fmt.Sprintf("(cd %s && sudo patch -f -p%d -R) < %s", p.BaseDir, p.Level, patchname)
			}

			err := k.Run(w, "ssh", rolesQuery(c), cmd)
			if err != nil {
				return fmt.Errorf("patch %s failed with error: %s", patchname, err)
			}
		}
		return nil
	}
}

func hup(c config.Config) func(io.Writer, knife.Knife) error {
	return func(w io.Writer, k knife.Knife) error {
		if c.Mode == config.ModeDryRun {
			return nil
		}

		fmt.Fprintf(w, "Sending HUP to all unicorns to reload the code with a concurrency of 1 and 30 seconds sleep...\n")

		return k.Run(w, "ssh", "-C", "1", rolesQuery(c), "echo 'reloading unicorn'; sudo gitlab-ctl hup unicorn; echo 'done reloading, sleeping'; sleep 30")
	}
}

func done(c config.Config) func(io.Writer, knife.Knife) error {
	return func(w io.Writer, _ knife.Knife) error {
		fmt.Fprintf(w, "Done!\n")
		return nil
	}
}

func rolesQuery(c config.Config) string {
	return strings.Join(mapF(c.GetRolesToApply(), func(v string) string {
		return fmt.Sprintf("roles:%s", v)
	}), " OR ")
}

func mapF(list []string, f func(string) string) []string {
	newList := []string{}
	for _, s := range list {
		newList = append(newList, f(s))
	}
	return newList
}
