package config_test

import (
	"os"
	"reflect"
	"testing"

	"gitlab.com/gl-infra/gitlab-patcher/config"
	"gitlab.com/gl-infra/gitlab-patcher/manifest"
)

var m = manifest.Manifest{
	Patches: map[string]manifest.Patch{
		"patch.diff": manifest.Patch{
			Checksum: "437b930db84b8079c2dd804a71936b5f",
			IssueURL: "https://gitlab.com/issues",
		},
	},
	Versions: map[string]manifest.Version{
		"0.9": manifest.Version{
			Environments: map[string][]string{
				"prod": []string{"lbs", "web-fe"},
			},
			Patches: []string{
				"patch.diff",
			},
		},
	},
}

func TestConfiguration(t *testing.T) {

	tt := []struct {
		name            string
		version         string
		environment     string
		mode            string
		expectedPatches []string
		expectedRoles   []string
	}{
		{"simple", "0.9", "prod", config.ModeDryRun, []string{"patch.diff"}, []string{"lbs", "web-fe"}},
		{"invalid version", "0.8", "prod", config.ModeRollback, nil, nil},
		{"invalid version", "0.9", "pre", config.ModePatch, nil, nil},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			c := config.New(m, tc.version, tc.environment, mustWorkingDir(t), tc.mode)
			assertEquals(t, "mode", tc.mode, c.Mode)
			assertEquals(t, "patches", tc.expectedPatches, c.GetPatchesToApply())
			assertEquals(t, "roles", tc.expectedRoles, c.GetRolesToApply())
		})
	}
}

func assertEquals(t *testing.T, msg, expected, actual interface{}) {
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("%s expected %v; got %v", msg, expected, actual)
	}
}

func mustWorkingDir(t *testing.T) string {
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}
	return wd
}
